## Writing tips

* Slides are ordered according to filename, therefore, name your slides, for example:
  * `00-title.md`
  * `01-find-a-destination.md`
  * `02-buy-a-bus-ticket.md`
  * `03-get-on-the-bus.md`
  * `04-get-off-the-bus-at-your-destination.md`
* Give the presentation a title, and optionally a subtitle, in the first slide
  <pre>
    <code class="nohighlight">
    ---
    title: Catching a bus
    subtitle: buses are cool!
    ---
    </code>
  </pre>

---

## Writing tips

* Give each subsequent slide a title by prefixing the first line with two hashes
  <pre>
    <code class="nohighlight">
      ## Find a Destination
    </code>
  </pre>
* Make each bullet point by prefixing each line with an asterisk
  <pre>
    <code class="nohighlight">
      ## Find a Destination

      * where do we want to go?
      * how far away is it?
    </code>
  </pre>

---

## Writing tips

* To make text <strong>bold</strong>, surround it with a <code>strong</code> tag.
  <pre>
    <code class="nohighlight">
      * do <strong>not</strong> get hit by the bus.
    </code>
  </pre>
* To make text <em>italic</em>, surround it with an <code>em</code> tag.
  <pre>
    <code class="nohighlight">
      * get on the bus <em>(at the bus stop)</em>
    </code>
  </pre>
* to <span class="fragment highlight-red">colour text</span>, surround it with a <code>span</code> tag, with a <code>class</code> attribute
  <pre>
    <code class="nohighlight">
      * Catch the <span class="fragment highlight-red">red</span> bus
    </code>
  </pre>

---

## Writing tips

* To create a link, put the text [inside square brackets] and the link (inside parentheses)
  <pre>
    <code class="nohighlight">
      * buy a [bus ticket](http://bustickets.com)
    </code>
  </pre>
* To link to an image, start with an exclamation mark and put the image text [inside square brackets] and the link (inside parentheses)
  <pre>
    <code class="nohighlight">
      * buy a ![image of a bus ticket](http://bustickets.com/ticket.jpg)

    </code>
  </pre>

