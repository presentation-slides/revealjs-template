## Making edits to slides using a web browser

* select which set of slides to edit.

* click on the `src` directory, then click which slide file you wish to edit.

  <img src="https://presentation-slides-media.gitlab.io/gitlab-example/gitlab-slide-repo-dirlisting.png" alt="Gitlab slide repository directory listing" width="280px" style="position: relative; display: inline-block; margin-left: 1%"/>

* click on <strong>Edit</strong>

  <img src="https://presentation-slides-media.gitlab.io/gitlab-example/gitlab-edit-file.png" alt="Gitlab Edit file button" width="280px" style="position: relative; display: inline-block; margin-left: 1%"/>

---

## Making edits to slides using a web browser

* make your changes to the file in the editor.

* enter in a <strong>Commit message</strong> that describes your change.

* click <strong>Commit changes</strong>.

  <img src="https://presentation-slides-media.gitlab.io/gitlab-example/gitlab-commit-changes.png" alt="Gitlab commit changes" width="280px" style="position: relative; display: inline-block; margin-left: 1%"/>

* the commit triggers the server to rebuild the slides, including your change.

* in about 5 minutes, your changes should be live in the slides.

---

## Making edits using your own computer

* Clone the appropriate slides repository
  * `git clone git@gitlab.com:presentation-slides-media/$name.git`

* Update dependent submodules
  * `git submodule update --recursive --init`

* Build the slides
  * `./bin/build.sh`

* Open the slides with a web browser in the `public` directory
  * e.g. `firefox public/index.html`

---

