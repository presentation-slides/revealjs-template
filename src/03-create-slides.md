## Creating new slides

###### Create a repository

* create a repository called `$name` under [https://gitlab.com/presentation-slides](https://gitlab.com/presentation-slides)
* clone that repository to a local disk.
  * `git clone git@gitlab.com:presentation-slides-media/$name.git`
* change directory to the cloned repository.
  * `cd $name`
* add the `reveal-js` template repository as a git submodule and make it relative.
  * `git submodule add https://gitlab.com/presentation-slides/revealjs-template`
  * `sed -i 's/https:\/\/gitlab.com\/presentation-slides-media/../g' .gitmodules`
  * `git submodule update --recursive --init`
* create a `.gitignore` file.
  * `echo -e "/result\n/public" > .gitignore`
* create a build file and make it executable.
  * `mkdir -p bin`
  * `echo -e "#\!/bin/sh\\n\\n(cd revealjs-template && sh ./bin/build.sh ../public ../src)" > bin/build.sh`
  * `chmod +x bin/build.sh`
* create a gitlab CI file.
  * `cp revealjs-template/.gitlab-ci.yml .`

---

