## Troubleshooting

* **Problem:** when building slides, `sh: 0: Can't open ./bin/build.sh`
* **Solution:** make sure dependent modules are updated: `git submodule update --recursive --init`

---

