## Making slides

* make a source directory for the slides.
  * `mkdir -p src`
* create slide files in the source directory, compiled in order of their filename.
  * use the revealjs documentation
    * [README](https://github.com/hakimel/reveal.js/)
    * [Examples](https://github.com/hakimel/reveal.js/wiki/Example-Presentations)
    * [Articles & Tutorials](https://github.com/hakimel/reveal.js/wiki/Articles-&-Tutorials)
* Run the build file open the slides in the `public` directory with a web browser.

---

