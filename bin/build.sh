#!/bin/sh

if [ -z "$1" ]
  then
    dist_dir=public
  else
    dist_dir="$1"
fi

if [ -z "$2" ]
  then
    src_dir=src
  else
    src_dir="$2"
fi

mkdir -p ${dist_dir}
mkdir -p ${dist_dir}/images

rsync -aH etc/ ${dist_dir}
rsync -aH reveal.js ${dist_dir}

cat ${src_dir}/* > ${dist_dir}/slides.md

pandoc -i -t revealjs --slide-level=2 --template=./template.revealjs --metadata=theme:white \
  --variable=revealjs-url=reveal.js \
  --variable=fragments:false \
  --variable=history:true \
  --variable=transition:none \
  --variable=pdfSeparateFragments:false \
  --no-highlight -s ${dist_dir}/slides.md -o ${dist_dir}/index.html
